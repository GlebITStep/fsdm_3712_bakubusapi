﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BakuBusApi.Controllers
{
    public class BakuBusApiResponse
    {
        public List<BUS> BUS { get; set; }
    }

    public class BUS
    {
        [JsonProperty("@attributes")]
        public Attributes attributes { get; set; }
    }

    public class Attributes
    {
        public string BUS_ID { get; set; }
        public string PLATE { get; set; }
        public string DRIVER_NAME { get; set; }
        public string CURRENT_STOP { get; set; }
        public string PREV_STOP { get; set; }
        public string SPEED { get; set; }
        public string BUS_MODEL { get; set; }
        public string LATITUDE { get; set; }
        public string LONGITUDE { get; set; }
        public string ROUTE_NAME { get; set; }
        public string LAST_UPDATE_TIME { get; set; }
        public string DISPLAY_ROUTE_CODE { get; set; }
        public string SVCOUNT { get; set; }
    }


    [Route("api/[controller]")]
    [ApiController]
    public class BakuBusController : ControllerBase
    {
        HttpClient httpClient = new HttpClient();

        [HttpGet("BusNumbers")]
        public async Task<IActionResult> GetBusNumbers()
        {
            var json = await httpClient.GetStringAsync("https://www.bakubus.az/az/ajax/apiNew1");
            BakuBusApiResponse data = JsonConvert.DeserializeObject<BakuBusApiResponse>(json);
            return new JsonResult(data.BUS
                .Select(x => x.attributes.DISPLAY_ROUTE_CODE)
                .Distinct()
                .OrderBy(x => x));
        }


        [HttpGet("Busses")]
        public async Task<IActionResult> GetBusses()
        {
            var json = await httpClient.GetStringAsync("https://www.bakubus.az/az/ajax/apiNew1");
            json = json.Replace("9,", "9.");
            json = json.Replace("0,", "0.");
            dynamic data = JsonConvert.DeserializeObject(json);
            return new JsonResult(data);
        }

        [HttpGet("BussesNew")]
        public async Task<IActionResult> GetBussesNew()
        {
            var json = await httpClient.GetStringAsync("https://www.bakubus.az/az/ajax/apiNew1");
            json = json.Replace("9,", "9.");
            json = json.Replace("0,", "0.");
            BakuBusApiResponse data = JsonConvert.DeserializeObject<BakuBusApiResponse>(json);
            return new JsonResult(data.BUS.Select(x => x.attributes));
        }

        //[HttpGet("Gleb")]
        //public IActionResult GetGleb()
        //{
        //    return new JsonResult(new { Name = "Gleb", Age = 25 });
        //}

        //[HttpGet("Ceyhun")]
        //public IActionResult GetCeyhun()
        //{
        //    return new JsonResult(new { Name = "Ceyhun", Age = 22 });
        //}
    }
}